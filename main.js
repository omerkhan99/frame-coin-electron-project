var electron = require('electron'),
    path = require('path')
;

let win = null;

electron.app.on('ready', ()=>{

    win = new electron.BrowserWindow({minWidth: 1000, width: 1000, minHeight: 700, height: 700});

    win.loadURL(`file://${path.join(__dirname, 'app/index.html')}`);

    win.on('closed', ()=>{

        win = null;
    });
});

electron.app.on('window-all-closed', electron.app.quit);